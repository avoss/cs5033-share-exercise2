package re.sha.toys.model.jpa;

import javax.persistence.EntityManager;

import bitronix.tm.BitronixTransactionManager;
import re.sha.model.jpa.JpaModel;

public class ToyModel extends JpaModel {

  public ToyModel(EntityManager entityManager, BitronixTransactionManager transactionManager) {
    super(entityManager, transactionManager);
  }
  
  // TODO: @Override any methods that need changing, especially those that create
  // data access objects that we have inherited from. You may also want to create
  // methods for retrieving objects with the type ToyCatalogueEntry and ToyUser
  // to avoid having to cast them later on (though the latter is acceptable).
}
