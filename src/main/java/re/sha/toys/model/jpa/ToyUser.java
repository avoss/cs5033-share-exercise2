package re.sha.toys.model.jpa;

import java.time.LocalDate;
import javax.mail.internet.AddressException;
import re.sha.model.jpa.JpaUser;

public class ToyUser extends JpaUser {
  
  /**
   * For Hibernate.
   */
  ToyUser() {
    super();
  }

  /**
   * A Toy Share User needs a title, name, surname, email as well as the birthday of their child.
   */
  ToyUser(String title, String name, String surname, String email, LocalDate boc) throws AddressException {
    super(title, name, surname, email);
    // TODO
  }
}
