package re.sha.toys.model.jpa;

import re.sha.model.jpa.JpaCatalogueEntry;

public class ToyCatalogueEntry extends JpaCatalogueEntry {
 
  /**
   * For Hibernate.
   */
  ToyCatalogueEntry() {
    super();
  }

  /**
   * A ToyCatalogueEntry requires an EAN number and a minimum age. 
   * https://en.wikipedia.org/wiki/International_Article_Number_(EAN)
   */
  ToyCatalogueEntry(String ean, int minAge) {
    super("ean", ean);
  }
  
}
