package re.sha.toys.business;

import re.sha.business.basic.BasicLendingRules;
import re.sha.exceptions.NoHoldingException;
import re.sha.exceptions.ShareException;
import re.sha.exceptions.TooManyItemsException;
import re.sha.model.CatalogueEntry;
import re.sha.model.LendingRecord;
import re.sha.model.Model;
import re.sha.model.User;
import re.sha.toys.model.jpa.ToyModel;

public class ToyLendingRules extends BasicLendingRules {
  
  ToyModel model = null; // version of the model that is already the appropriate type, set in setModel(Model)

  @Override
  public LendingRecord borrow(User user, CatalogueEntry entry)
      throws NoHoldingException, TooManyItemsException, ShareException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void setModel(Model model) {
    if(model instanceof ToyModel) {
      this.model = (ToyModel) model;
      super.model = model;
    } else {
      throw new RuntimeException("Got a model that is not a ToyModel!");
    }
  }
}
