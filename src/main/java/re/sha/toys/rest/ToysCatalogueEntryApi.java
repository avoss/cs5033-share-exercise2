package re.sha.toys.rest;

import javax.ws.rs.Path;

/**
 * Implements RESTful API for toys. Note that the REST API for {@link CatalogueEntry}
 * can stay in place and continue to be used. The endpoints here are simply more specific
 * and allow the handling of the images. 
 *  
 * @author alex.voss@st-andrews.ac.uk
 */
@Path("/toy")
public class ToysCatalogueEntryApi {
   
}
